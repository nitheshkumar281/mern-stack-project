import './App.css';
import {BrowserRouter,Routes,Route} from "react-router-dom"
import Loginform from './Components/Loginform'

import Home from './Components/Home';
import Contactus from './Components/Contactus';
import Aboutus from './Components/Aboutus';
import Registrationform from './Components/Registrationform';
import Products from "./Components/Products"


function App() {
  return (
    <div className='App'>
     <BrowserRouter>
    {/* <Navbar/> */}
    {/* <Products/> */}
    <Routes>
    <Route path="/" element={<Home/>} />
     <Route path="/loginform" element={<Loginform/>} />
     <Route path="/contact" element={<Contactus/>} />
     <Route path="/about" element={<Aboutus/>} />
     <Route path="/register" element={<Registrationform/>} />
     <Route path="/products" element={<Products/>} />
     {/* <Route path="/topbar" element={<Topbar/>} /> */}

    </Routes>
           </BrowserRouter>
    </div>
  );
}    

export default App;