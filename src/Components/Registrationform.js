import React ,{useState} from 'react'
import "../Css/registrationform.css"
import regbg from '../Images/loginbg.jpg'
import {Label,FormGroup,Input,Col,Form,Row,Button} from "reactstrap"
import axios from 'axios'
import { useNavigate } from 'react-router-dom';
import {Link} from 'react-router-dom'
import Navbar from '../Components/Navbar'

function Registrationform() {
    const navigate = useNavigate()
    let [formdata,setFormdata] = useState({
        email: "",
        password: "",
        address: "",
        address2:"",
        city: "",
        state: "",
        zip: ""
        
  })
  
    const handlesubmit = async(e) => {
      e.preventDefault();
      // alert(${formdata.email})
      navigate('/Loginform')
       try {
          let response = await axios.post("http://localhost:3005/register/postregister", formdata)
         console.log(response)
        //  alert(response.data)
        //  setFormdata("")
        if (response.data === "Successfully Registered") {
          navigate('/loginform')
        }
         if(response.data==="User Already Exists"){
          alert(`email already exists ! ,Register with new email id`)
          navigate("/register")
         }


         setFormdata({
          email: "",
        password: "",
        address: "",
        city: "",
        state: "",
        zip: ""
         })
        

      } catch (err) {
          console.log(err)
      }

    }
    const handleFormdata = (e) => {
        let { name, value }=e.target
        setFormdata({
            ...formdata,
            [name]:value
        })
        console.log(formdata)
    }

  return (
    <div>
      <Navbar/>
    <div className='bodydiv'>
       <img id='reg' src={regbg}></img>
      <div className='regouterdiv'>
      <div className='regformhead'>
         <h1>Register here!</h1>
      <Form className='regformdiv' onSubmit={handleFormdata}>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleEmail">
          Email
        </Label>
        <Input
          id="exampleEmail"
          name="email"
          placeholder="Email"
          type="email"
          value={formdata.email}
          onChange={ handleFormdata }
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="examplePassword">
          Password
        </Label>
        <Input
          id="examplePassword"
          name="password"
          placeholder="password"
          type="password"
          value={formdata.password}
          onChange={ handleFormdata }
          required
        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup>
    <Label for="exampleAddress">
      Address
    </Label>
    <Input
      id="exampleAddress"
      name="address"
      placeholder="1234 Main St"
      value={formdata.address}
          onChange={ handleFormdata }
          required
    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleAddress2">
      Address 2
    </Label>
    <Input
      id="exampleAddress2"
      name="address2"
      placeholder="Apartment, studio, or floor"
      value={formdata.address2}
          onChange={ handleFormdata }
          required
    />
  </FormGroup>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleCity">
          City
        </Label>
        <Input
          id="exampleCity"
          name="city"
          placeholder='city'
          value={formdata.city}
          onChange={ handleFormdata }
          required
        />
      </FormGroup>
    </Col>
    <Col md={4}>
      <FormGroup>
        <Label for="exampleState">
          State
        </Label>
        <Input
          id="exampleState"
          name="state"
          placeholder='state'
          value={formdata.state}
          onChange={ handleFormdata }
          required
        />
      </FormGroup>
    </Col>
    <Col md={2}>
      <FormGroup>
        <Label for="exampleZip">
          Zip
        </Label>
        <Input
          id="Zip"
          name="zip code"
          placeholder='pincode'
        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup check>
    <Input
      id="exampleCheck"
      name="check"
      type="checkbox"
    />
    <Label
      check
      for="exampleCheck"
    >
      Check me out
    </Label>
    
  </FormGroup>
  <Button className='submit' onClick={handlesubmit}>
    Register
  </Button>
</Form>
<Link to="/loginform"><p> Already Registered? Login Here</p></Link>
      </div>
      </div>
    </div>
    </div>
  )
}

export default Registrationform;
