import React from 'react'
import '../Css/aboutus.css'
import aboutusbg from'../Images/aboutusbg.mp4'
import editing from '../Images/editing.webp'
import music from '../Images/music.jpg'
import market from '../Images/marketing.jpg'
import Navbar from '../Components/Navbar'

function Aboutus() {
  return (
    <div>
      <Navbar/>
    <div className='outerbox'>
        <div className='aboutbg'>
       <video loop autoPlay muted playsInline src={aboutusbg}></video>
        </div>
       <div className='aboutdiscription'>
       <h1>Movie trailers are concise audio-visual previews designed to provide a glimpse into a film's content and style, enticing audiences to watch the full movie. These trailers serve as a crucial component of a film's marketing strategy and are typically released before the movie's premiere. </h1>
       </div>
       <div className='discription'>
        <h2>"Editing and pacing: Trailers are carefully edited to create a sense of rhythm and build tension. They often follow a structure that includes an introduction, rising action, climax, and conclusion."</h2>
        <img class='aboutimg' src={editing}></img>
       </div>
       <div className='music'>
        <img className='musicimg' src={music}></img>
        <h2>Music and sound effects: The use of music and sound effects is crucial in setting the tone and mood of the film. These elements enhance the emotional impact of the trailer.</h2>
       </div>
       <div className='market'>
        <h2>Marketing strategy: Movie studios strategically release trailers at specific times to maximize visibility and generate buzz. Teaser trailers may be released well in advance to build anticipation, while full-length trailers often come closer to the movie's release date.</h2>
        <img className='aboutimg' src={market}></img>
       </div>
    </div>
    </div>
  )
}

export default Aboutus