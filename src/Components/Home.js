import React, { useState, useEffect } from 'react';
import home1 from "../Images/aboutbg.mp4";
import home2 from "../Images/Home1.webm";
import home3 from "../Images/Home2.webm";
import "../Css/home.css"
import Navbar from "../Components/Navbar"

import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
} from 'reactstrap';
import { Link } from 'react-router-dom';

const items = [
  {
    src: home1,
    altText: 'Slide 1',
    caption: 'Slide 1',
  },
  {
    src: home2,
    altText: 'Slide 2',
    caption: 'Slide 2',
  },
  {
    src: home3,
    altText: 'Slide 3',
    caption: 'Slide 3',
  },
];



function Home(args) {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  useEffect(() => {
    const advanceToNextSlide = () => {
      const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
      setActiveIndex(nextIndex);
    };

    const timeoutId = setTimeout(() => {
      advanceToNextSlide();
    }, 10000);

    return () => clearTimeout(timeoutId);
  }, [activeIndex]);

  const slides = items.map((item, index) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={index}
      >
        <video loop autoPlay playsInline src={item.src} type="video/mp4" className='video'></video>
        <CarouselCaption

        />
      </CarouselItem>

    );
  });

  return (
    <div className='homeouterdiv'>
      <Navbar />

      <div className='carouselbody'>
        <Carousel
          activeIndex={activeIndex}
          next={next}
          previous={previous}
          {...args}
        >
          <CarouselIndicators
            items={items}
            activeIndex={activeIndex}
            onClickHandler={goToIndex}
          />
          {slides}
          <CarouselControl
            direction="prev"
            directionText="Previous"
            onClickHandler={previous}
          />
          <CarouselControl
            direction="next"
            directionText="Next"
            onClickHandler={next}
          />
        </Carousel>

      </div>
      <div className='homeheading'>
        <h1>CINEMATIC TEASERS</h1>
      </div>
      <div className="homefooter">
        <div className="homefooter1">
          <h2>Quick Contact Info</h2>
          <p>You can contact us for your any queries. Our Team will support you with pleasure.</p>
          <h5>3-108 , Gachibowli , Telangana , 503245</h5>
          <Link><h5>Movierz@gmail.com</h5></Link>
          <Link><h5>91-9005555211</h5></Link>
        </div>
        <div className="homefooter1">
          <h1>Pages</h1>
          <Link id="footlink"><h5>Home</h5></Link>
          <Link id="footlink"><h5>Our Features</h5></Link>
          <Link id="footlink"><h5>About Us</h5></Link>
          <Link id="footlink"><h5>Contact Us</h5></Link>
          <Link id="footlink"><h5>Privacy Policy</h5></Link>
          <Link id="footlink"><h5>Why us?</h5></Link>
        </div>
        <div className="homefooter1">
          <h2>Services</h2>
          <h5>Hyderabad</h5>
          <h5>Bangalore</h5>
          <h5>Chennai</h5>
          <h5>Coimbatore</h5>
          <h5>Delhi</h5>
          <h5>Kerala</h5>
        </div>

      </div>
    </div>

  );
}

export default Home;
