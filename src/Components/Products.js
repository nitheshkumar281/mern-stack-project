import React, { useState, useEffect } from 'react'
import axios from "axios"
import { Card, CardBody, CardTitle, Button} from "reactstrap"
import "../Css/products.css"
import Topbar from "../Components/Topbar"
import Carousels from "../Components/Carousels"
import { useNavigate } from 'react-router-dom';


function Cardspage() {
  let navigate = useNavigate()
  const [products, setProducts] = useState(null);

 

  const fetchdata = async () => {
    try {
      let response = await axios.get("http://localhost:3005/product") //api that should get data from api
      console.log(response)
      console.log(response.data)
      setProducts(response.data)
    }
    catch (err) {
      console.log(err)
    }
  }
  useEffect(() => {
    fetchdata()
  }, []);
  // const displayMoreDetails =(index)=>{
  //   alert(index)
  // navigate('/moredetails',{state:{selectedIndex:index}})
  // }

  return (
    <div className='productmain'>
      <Topbar/>
      <Carousels/>
      <marquee direction="right" Loop="20" scrollamount="25"><h1 style={{color: 'white'}} >Telugu Movies</h1></marquee>
    <div className="d-flex p-2 m-2" id="productouterdiv">
      {products&&products.map((item) => (
        <div className='productcards'>
          <Card
            style={{
              width: '18rem'
            }}
          >
            <img
              alt="Sample"
              src={item.img}
            />
            <CardBody>
              <CardTitle tag="h5">
                {item.title}
              </CardTitle>
              <a href="https://youtu.be/rc61YHl1PFY?si=kzOMxfuRNG_b-V83"> <button>Play</button></a>

               </CardBody>
          </Card>

        </div>
      ))}
</div>
    </div>
  )
}

export default Cardspage;
