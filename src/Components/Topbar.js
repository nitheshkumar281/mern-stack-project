import React from 'react'
import "../Css/navbar.css"
import { Link } from "react-router-dom"
// import navbar from "../Images/navbar.mp4"
import logo from "../Images/logo.png"

function Navbar() {
    return (
        <div className="outerdiv">
            <div className='inner1'>
                
                <Link to="/home"><img src={logo} alt="logo" /></Link>
            </div>
            <div className='inner2'>
                {/* <Link id='cont' to="/contact"  ><h5>Contactus</h5></Link> */}
                <Link id='abt' to="/about"><h5>Aboutus</h5></Link>
                <Link id='log'  to="/"><h5>Logout</h5></Link>
                {/* <Link id='reg' to="/register"><h5>Register</h5></Link> */}
            </div>
        </div>
    )
}

export default Navbar