const mongoose = require('mongoose');
const Grid = require('gridfs-stream');
const fs = require('fs');

// Initialize MongoDB connection
const conn = mongoose.createConnection('mongodb://localhost/mydatabase');

let gfs;

conn.once('open', () => {
  // Initialize GridFS stream
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection('videos'); // Name of the collection where videos will be stored

  // Function to upload video
  const uploadVideo = (filename, filePath) => {
    const writestream = gfs.createWriteStream({
      filename: filename
    });

    const readStream = fs.createReadStream(filePath);
    readStream.pipe(writestream);
  };

  // Example usage
  uploadVideo('example.mp4', '/path/to/example.mp4');
});
