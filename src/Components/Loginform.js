import React, { useState } from 'react'
import "../Css/loginform.css"
import loginbg from "../Images/cam.jpg"
import emailbg from "../Images/mail1.jpg"
import password from "../Images/pin.jpg"
import axios from 'axios'
import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router-dom';
import Navbar from '../Components/Navbar'

function Loginform() {
  const navigate = useNavigate()
  const [formdata, setformdata] = useState({
    email: "",
    password: "",
  });

  const handleformdata = async (e) => {
    let { name, value } = e.target

    setformdata({
      ...formdata,
      [name]: value
    })  
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    // alert(`${formdata.email} and ${formdata.password}  `)
    console.log(formdata)

    try {
      let response = await axios.post('http://localhost:3005/login', formdata)
      console.log(response.data)

      if (response.data === "Login successfull") {
        alert("login successful")
        navigate("/products")
      }

      if (response.data === "invalid password") {
        alert(`invalid password`)
        setformdata({
          email: "",
          password: ""
        })
        navigate("/loginform")
      }

      if (response.data === "User not found") {
        alert(`email doesn't exists`)
        setformdata({
          email: "",
          password: ""
        })
        navigate("/loginform")
      }
      }
    catch (err) {
        console.log(err)
      }

    }

  return (
      <div>
        <Navbar/>
        <div className='bodydiv'>
          <img id='bgimg' alt="" src={loginbg}></img>

          <div className='formdiv'>
            <form className="loginbox" onSubmit={handleSubmit}>
              <h1><b>Hello! Welcome... </b></h1>
              <h3>login to your account</h3>
              <label>Email_I'd</label>
              <div className="inputs">
                <input type="email" placeholder='Enter email' name="email" value={formdata.email}
                  onChange={handleformdata} required></input>
              </div>
              <label>Password</label>
              <div className="inputs">
                <input type="password" placeholder='Enter password' name="password" value={formdata.password}
                  onChange={handleformdata} required></input>
              </div>

              <div className="checkbox">
                <input type="checkbox"></input>
                <label>RememberMe</label>
              </div>
              <button id="buttons" type="submit">Login</button>
              <Link to="/register"><p>Don't have an Account? Register</p></Link>
            </form>
          </div>
        </div>
      </div>
    )
  }

  export default Loginform
